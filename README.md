# Car Parking

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)


## Introduction

Car Parking is a java based simple application capable of providing car parking functionality based on ticket they get while entering the parking lot. The parking charge calculated based on the time a user occupies a parking spot.

### Prerequisites

This application uses basic concepts of java and doesn't need any advanced knowledge and skills. Basic understanding of OOP concept along with how to build and run the java applicaiton would be sufficient for this project.

