/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygroup.beans;

import com.mygroup.connection.ConnectionManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Aspire
 */
public class User {

    public String TABLENAME = "users";

    private String fullname;
    private int id;
    private String role;
    private String cardNumber;
    private String expiresOn;

    public User(String name, String cardNumber) {
        this.fullname = name;
        this.cardNumber = cardNumber;
    }

    public User() {
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(String expiresOn) {
        this.expiresOn = expiresOn;
    }

    public static boolean isValid(String cardNumber) {
        User user = new User().getUser(cardNumber);
        return user != null;
    }

    public  User getUser(String cardNumber) {
        try {
            Connection con = ConnectionManager.getConnection();
            Statement stmt;
            ResultSet rs;
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM users WHERE card_number = '" + cardNumber + "'");
            while (rs.next()) {
                User user = new User();
                user.setFullname(rs.getString("full_name"));
                user.setCardNumber(rs.getString("card_number"));
                user.setExpiresOn(rs.getString("expires_on"));
                user.setId(Integer.parseInt(rs.getString("id")));
                user.setRole(rs.getString("role"));
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("User not found");
            return null;
        }
        System.out.println("User not found");
        return null;
    }

}
