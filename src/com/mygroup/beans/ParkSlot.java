/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygroup.beans;

import com.mygroup.front.Logger;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

/**
 *
 * @author Aspire
 */
public class ParkSlot {

    private static final int SLOTCOUNT = 12;
    private long checkin;
    private long checkout;
    private int ticketNo;
    private int slotNumber;
    static HashMap<Integer, ParkSlot> slots;
    private int pricingPlan;
    private double cost;

    private ParkSlot(int i, long checkin, long checkout, int ticketno) {
        this.slotNumber = i;
        this.checkin = checkin;
        this.checkout = checkout;
        this.ticketNo = ticketno;
    }

    public long getCheckin() {
        return checkin;
    }

    public void setCheckin(int checkin) {
        this.checkin = checkin;
    }

    public long getCheckout() {
        return checkout;
    }

    public void setCheckout(long checkout) {
        this.checkout = checkout;
    }

    public int getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(int ticketNo) {
        this.ticketNo = ticketNo;
    }

    public int getPricingPlan() {
        return pricingPlan;
    }

    public void setPricingPlan(int pricingPlan) {
        this.pricingPlan = pricingPlan;
    }

    public static void displayParkSlots() {
        // slots = new HashMap<>();
        if (ParkSlot.slots == null) {
            slots = new HashMap<>();
            for (int i = 1; i <= ParkSlot.SLOTCOUNT; i++) {
                ParkSlot p = new ParkSlot(i, 0, 0, 0); //slotno, checkin, checkout, ticketno
                ParkSlot.slots.put(i, p);
            }
        }
        Logger.log("size is " + ParkSlot.slots.size());
        int count = 1;

        Iterator it = ParkSlot.slots.entrySet().iterator();
        System.out.println("Available slots are ");
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            ParkSlot p = (ParkSlot) pair.getValue();
            if (p.ticketNo == 0) {
                System.out.print("| " + (p.slotNumber < 10 ? "0" + p.slotNumber : p.slotNumber) + " |\t");
            } else {
                System.out.print("| X |\t");
            }
            if (count % 4 == 0) {
                System.out.println();
            }
            count++;
        }
    }

    //reserve specific slot
    public static ParkSlot reserveSlot(int slotNumber, int ticketNumber, int pricingPlan) {
        ParkSlot p = new ParkSlot(slotNumber, getCurrentTime(), 0, ticketNumber);
        p.setPricingPlan(pricingPlan);
        ParkSlot.slots.put(slotNumber, p);
        displayParkSlots();
        return p;
    }
    
    public static ParkSlot freeOutSlot(ParkSlot p) {
        p.setTicketNo(0);
        ParkSlot.slots.put(p.getSlotNumber(), p);
        displayParkSlots();
        return p;
    }

    public static ParkSlot getReservedSlot(int ticketNumber) {
        Iterator it = ParkSlot.slots.entrySet().iterator();
        System.out.println("Available slots are ");
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            ParkSlot p = (ParkSlot) pair.getValue();
            if (p.ticketNo == ticketNumber) {
                return p;
            }
        }
        return null;
    }

    public static ParkSlot setPreCheckoutParkSlot(int slotNo, double cost, long checkout) {
        ParkSlot p = ParkSlot.slots.get(slotNo);
        p.setCost(cost);
        p.setCheckout(checkout);
        ParkSlot.slots.put(slotNo, p);
        return p;
    }

    public static long getCurrentTime() {
       // return (int) System.currentTimeMillis()*1000;// * -1;
       //return (long) System.currentTimeMillis();// / 1000L;
       return Instant.now().getEpochSecond();
    }

    public static String getReadableTime(long unixSeconds) {
        Date date = new Date(unixSeconds*1000); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-6")); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public int getSlotNumber() {
        return slotNumber;
    }

    public void setSlotNumber(int slotNumber) {
        this.slotNumber = slotNumber;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    public double getCost() {
        return this.cost;
    }

}
