/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygroup.front;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Aspire
 */
public class EntryPoint {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        EntryPoint main = new EntryPoint();
        while (true) {
            main.displayMenu();
            Scanner sc = new Scanner(System.in);
            int action = sc.nextInt(); //throws inputmismatchexcepton TODO
            if (action == 1) {
                int usertype = main.getUserType();
                Checkin checkin = new Checkin(usertype);
            } else if (action == 2) {
                System.out.println("Enter your ticket no :");
                int ticketNo = sc.nextInt(); //throws inputMismatchException
                new Checkout(ticketNo);
            } else {
                //invalid choice
            }
        }

    }

    public int getUserType() {
        boolean invalidInput = true;
        int menuChoice = 0;
        while (invalidInput) {
            Scanner scanner = new Scanner(System.in);
            try {
                checkinMenu();
                menuChoice = scanner.nextInt();
                if (menuChoice != 1 && menuChoice != 2) {
                    throw new InputMismatchException();
                }
                invalidInput = false;
            } catch (InputMismatchException ex) {
                System.out.println("Invalid Input. Please try again :");
                invalidInput = true;
            }
        }
        return menuChoice;

    }

    static void displayMenu() {
        // Date d = (Date) null;
        System.out.println("Welcodme to GCCP");
        System.out.println("1. Checkin");
        System.out.println("2. Checkout");
    }

    static void checkinMenu() {
        System.out.println("Please select a user type :");
        System.out.println("1. Permanent User");
        System.out.println("2. Sporadic User");
    }

}
