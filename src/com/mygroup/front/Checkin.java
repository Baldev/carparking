/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygroup.front;

import com.mygroup.beans.ParkSlot;
import com.mygroup.beans.User;
import com.mygroup.connection.ConnectionManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Aspire
 */
public class Checkin {

    int usertype;
    //ArrayList<User> users;

    public Checkin(int usertype) {
        this.usertype = usertype;
        init();
    }

    public void init() {
        getUser();
    }

    void getUser() {
        switch (usertype) {
            case 1:
                //user type is permanent
                User user = getEntrance();
                if (user != null) {
                    selectParkingSlot();
                } else {
                    System.out.println("Invalid user. No entrance");
                }
                break;
            case 2:
                selectParkingSlot();
                //user type is sporadic user
                break;

        }
    }

    void selectParkingSlot() {
        ParkSlot.displayParkSlots();
        System.out.print("Select a Slot Number :");
        Scanner sc = new Scanner(System.in);
        int slotNumber = sc.nextInt();

        //show rates
        //select plan
       int planId = selectPlan();
        //generate ticket 
        Random rand = new Random();
        int ticketNo = (int) System.currentTimeMillis() * -1;
        //Logger.log("ticketNo is " + ticketNo);
        
        ParkSlot reservedSlot = ParkSlot.reserveSlot(slotNumber,ticketNo,planId);
        //reserve slot
        System.out.println("\n Your parking ticket : ");
        System.out.println("+--------------------------------+");
        System.out.println("| Ticket no : "+ticketNo);
        System.out.println("| Checkin time : "+ParkSlot.getReadableTime(reservedSlot.getCheckin()));
        System.out.println("| Slot number : "+reservedSlot.getSlotNumber());
        System.out.println("+--------------------------------+\n");

    }

    int selectPlan() {
        showParkingRates();
        Scanner sc = new Scanner(System.in);
        int parkingPlan = sc.nextInt(); //handle input mismatch exception
        return parkingPlan;

    }

    void showParkingRates() {
        System.out.println("\n\nSelect Plan : ");
        System.out.println("1. Hourly : $"+Checkout.baseCost+" for 1 hour, $"
                +Checkout.hourlyCost+" per additional hours");
        System.out.println("2. Whole day : $"+Checkout.wholeDayCost);
        System.out.print("Your choice : ");
    }

    public User getEntrance() {
        Scanner sc = new Scanner(System.in);
        User user = null;
        if (usertype == 1) {
            // while (user == null) {
            System.out.println("Enter card number :");
            String cardNumber = sc.next();
            if (cardNumber.equals("q")) {
                System.out.println("Thanks");
                System.exit(0);
            }
            if (User.isValid(cardNumber)) {
                user = new User().getUser(cardNumber);
                System.out.println(user.getFullname());
                   //1 users.add(user);
                // break;
            } else {
                System.out.println("Record not found. Try again :");
            }
            // }

        } else {

            System.out.println("Enter name :");
            String name = sc.next();
            System.out.println("Enter card number :");
            String cardNumber = sc.next();

            User newuser = new User(name, cardNumber);
        }
        return user;
    }

    public void mainmenu() {
        try {
            Connection con = ConnectionManager.getConnection();
            Statement stmt = null;
            ResultSet rs = null;
            boolean found = false;

            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM users WHERE card_number = '123456789'");
            while (rs.next()) {
                found = true;
                break;
            }
            if (found) {
                System.out.println("Logged in as " + rs.getString("full_name"));
            } else {
                System.out.println("record not found");
            }
        } catch (SQLException ex) {
            System.out.println("error occured " + ex.getMessage());
        }

    }
}
