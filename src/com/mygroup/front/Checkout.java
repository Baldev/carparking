/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygroup.front;

import com.mygroup.beans.ParkSlot;

/**
 *
 * @author Aspire
 */
public class Checkout {

    private int ticketNo;
    private double cost;
    private long checkout;
    public static int baseCost = 10;
    public static int hourlyCost = 5;
    public static int wholeDayCost = 25;

    Checkout(int ticketNo) {
        this.ticketNo = ticketNo;
        showCharge();
    }

    void makePayment() {
        ParkSlot p = ParkSlot.getReservedSlot(ticketNo);
        showCheckoutDetail(p);
       
        System.out.println("You payment is successful. \nThank you");
        
        freeOutSlot(p);
    }

    void showCheckoutDetail(ParkSlot p){
        System.out.println("\nYour parkfing detail are :");
        System.out.println("+------------------------------+");
        System.out.println("| Ticket no :"+p.getTicketNo());
        System.out.println("| Checkin time : "+ParkSlot.getReadableTime(p.getCheckin()));
        System.out.println("| Checkout Time : "+ParkSlot.getReadableTime(p.getCheckout()));
        System.out.println("| Total cost : $"+p.getCost());
        System.out.println("+------------------------------+");
    }
    void freeOutSlot(ParkSlot p) {
        p.setTicketNo(0);
        ParkSlot.freeOutSlot(p);
                
    }

    private void showCharge() {
        ParkSlot reservedSlot = ParkSlot.getReservedSlot(this.ticketNo);
        long now = ParkSlot.getCurrentTime();
        //setCheckout(now);
        long timedifference = now - reservedSlot.getCheckin();
        int hour = (int) Math.ceil(timedifference / 3600.0 + 0.5);
        if (reservedSlot.getPricingPlan() == 1) {
            if (hour <= 1) {
                setCost(baseCost);
            } else {
                setCost(baseCost + ((hour - 1) * hourlyCost));
            }
        }else{
            setCost(wholeDayCost);
        }

        ParkSlot p = ParkSlot.setPreCheckoutParkSlot(reservedSlot.getSlotNumber(), getCost(), now);
        makePayment();
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public long getCheckout() {
        return checkout;
    }

    public void setCheckout(int checkout) {
        this.checkout = checkout;
    }

}
